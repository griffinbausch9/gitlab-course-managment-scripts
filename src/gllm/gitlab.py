import os
import requests


class GitLab:
    def __init__(self, gitlabUsername, gitlabURL, gitlabToken):
        self.username = gitlabUsername
        self.url = gitlabURL
        self.token = gitlabToken

    def removeUserFromProject(self, userId, projectId):
        url = self.getBaseUrl() + 'projects/' + str(projectId) + '/members/' + userId
        requests.delete(url, params=self.getBasePayload())

    def getGroupProjects(self, groupId):
        url = self.getBaseUrl() + 'groups/' + str(groupId) + '/projects?per_page=100'
        return requests.get(url, params=self.getBasePayload())

    def getGroupProjectIdByName(self, groupId, projectName):
        url = self.getBaseUrl() + 'groups/' + str(groupId) + '/projects'
        projects = requests.get(url, params=self.addToBasePayload('search', projectName))n
        return next((x['id'] for x in projects.json() if x['path'] == projectName), None)

    def getProjectUsers(self, projectId):
        url = self.getBaseUrl() + 'projects/' + str(projectId) + '/users'
        return requests.get(url, params=self.getBasePayload())

    def getForks(self, projectId):
        url = self.getBaseUrl() + 'projects/' + str(projectId) + '/forks?per_page=100'
        return requests.get(url, params=self.getBasePayload())

    def getUserId(self, gitlabUsername):
        url = self.getBaseUrl() + 'users'
        userInfo = requests.get(url, params=self.addToBasePayload('username', gitlabUsername))
        return userInfo.json()[0]['id']

    def getGroupId(self, groupName):
        url = self.getBaseUrl() + 'groups'
        groupInfo = requests.get(url, params=self.addToBasePayload('search', groupName))
        return groupInfo.json()[0]['id']

    def cloneForks(self, projectId, directory):
        forks = self.getForks(projectId)
        for fork in forks.json():
            cloneName = self.generateCloneName(fork)
            self.clone(fork['ssh_url_to_repo'], cloneName, directory)

    def generateCloneName(self, fork):
        forkId = fork['id']
        users = self.getProjectUsers(forkId).json()
        cloneName = fork['owner']['username']
        for user in users:
            if user['username'] != fork['owner']['username'] and self.username != user['username']:
                cloneName = cloneName + '-' + user['username']
        return cloneName

    def addToBasePayload(self, key, value):
        payload = self.getBasePayload()
        payload[key] = value
        return payload

    def getBaseUrl(self):
        return self.url + 'api/v4/'

    def getBasePayload(self):
        return {'private_token': self.token}

    def clone(self, projectUrl, cloneName, directory):
        os.chdir(directory)
        os.system('git clone ' + projectUrl + ' ' + cloneName)
