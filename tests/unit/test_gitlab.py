import pytest
import gllm.gitlab


# requests-mock registers requests_mock as a fixture automatically
def test_removeUserFromProject(requests_mock, baseUrl, baseQuery, gitlab):
    requests_mock.delete(f'{baseUrl}projects/PROJECT/members/USER?{baseQuery}')
    gitlab.removeUserFromProject('USER', 'PROJECT')
    # no assert; requests-mock will explode if URI is wrong.


def test_getGroupProjects(requests_mock, baseUrl, baseQuery, gitlab):
    requests_mock.get(f'{baseUrl}groups/GROUP/projects?per_page=100&{baseQuery}')
    assert gitlab.getGroupProjects('GROUP').status_code == 200


def test_getGroupProjectIdByName(requests_mock, baseUrl, baseQuery, gitlab):
    url = f'{baseUrl}groups/GROUP/projects?{baseQuery}&search=PROJECT'
    json = [{'name':'PROJECT', 'id':'ID'}]
    requests_mock.get(url, json=json)
    assert gitlab.getGroupProjectIdByName('GROUP', 'PROJECT') == 'ID'


def test_getProjectUsers(requests_mock, baseUrl, baseQuery, gitlab):
    requests_mock.get(f'{baseUrl}projects/PROJECT/users?{baseQuery}')
    assert gitlab.getProjectUsers('PROJECT').status_code == 200


def test_getForks(requests_mock, baseUrl, baseQuery, gitlab):
    requests_mock.get(f'{baseUrl}projects/PROJECT/forks?{baseQuery}&per_page=100')
    assert gitlab.getForks('PROJECT').status_code == 200


def test_getUserId(requests_mock, baseUrl, baseQuery, gitlabUsername, gitlab):
    requests_mock.get(f'{baseUrl}users?{baseQuery}&username={gitlabUsername}', json=[{'id':'ID'}])
    assert gitlab.getUserId(gitlabUsername) == 'ID'


def test_getGroupId(requests_mock, baseUrl, baseQuery, gitlab):
    requests_mock.get(f'{baseUrl}groups?{baseQuery}&search=GROUP', json=[{'id':'ID'}])
    assert gitlab.getGroupId('GROUP') == 'ID'


def test_generateCloneName(requests_mock, baseUrl, baseQuery, gitlab):
    fork = {
        'id': 'FORK',
        'owner': {
            'username': 'OWNER',
        }
    }

    project_users = [
        {
            'username': 'OWNER',
        },
        {
            'username': 'USER2',
        },
        {
            'username': 'bogus-user',
        },
    ]

    requests_mock.get(f'{baseUrl}projects/FORK/users?{baseQuery}', json=project_users)

    gitlab.config = {'gitlabUsername': 'INSTRUCTOR'}

    print(f'{baseUrl}projects/FORK/users?{baseQuery}')
    assert gitlab.generateCloneName(fork) == 'OWNER-USER2'


@pytest.fixture()
def gitlab(gitlabUsername, gitlabUrl, gitlabToken):
    return gllm.gitlab.GitLab(gitlabUsername, gitlabUrl, gitlabToken)


@pytest.fixture
def gitlabUrl():
    return 'https://bogus.server/'


@pytest.fixture
def gitlabUsername():
    return 'bogus-user'


@pytest.fixture
def gitlabToken():
    return 'bogus-token'


@pytest.fixture
def baseQuery(basePayload):
    return dictToQueryString(basePayload)


@pytest.fixture()
def baseUrl(gitlabUrl):
    return f'{gitlabUrl}api/v4/'


@pytest.fixture()
def basePayload(gitlabToken):
    return {'private_token': gitlabToken}


def dictToQueryString(payload):
    return '&'.join(f'{k}={v}' for k,v in payload.items())
