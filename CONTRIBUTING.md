
Requirements for development
-----------------------------

* Python 3.7
* Pipenv


Setting up development environment
----------------------------------

1. Fork this repository
2. Clone your fork
3. Install development environment
    ```
    pipenv install --dev
    ```


Running the tests
-----------------

```
pipenv run tox
```


Viewing coverage data
---------------------

After running tests, open `tests/htmlcov/index.html` in a browser.
